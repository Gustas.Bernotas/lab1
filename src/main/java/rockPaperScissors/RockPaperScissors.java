package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {

        while (true) {
            System.out.println("Let's play round " + roundCounter);
            String humanChoice = playerChoice("Your choice (Rock/Paper/Scissors)?", rpsChoices);
            String pcChoice = computerChoice();
            if (theWinner(humanChoice, pcChoice)) {
                humanScore ++;
                System.out.println("Human chose " + humanChoice + ", computer chose " + pcChoice + ". Human wins!");

            }
            else if (theWinner(pcChoice, humanChoice)) {
                computerScore ++;
                System.out.println("Human chose " + humanChoice + ", computer chose " + pcChoice + ". Computer wins!");
            }
            else{
                System.out.println("Human chose " + humanChoice + ", computer chose " + pcChoice + ". It's a tie!");

            }
            System.out.println("Score: human " + humanScore + ", computer " +computerScore);
            
            String exitGame = playerChoice("Do you wish to continue playing? (y/n)?", Arrays.asList("y","n"));
            if (exitGame.equals("n")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter ++;
      }
    }
    public boolean theWinner(String choice_1, String choice_2) {
        if (choice_1.equals("paper")) {
            return choice_2.equals("rock");
        }
        else if (choice_1.equals("rock")) {
            return choice_2.equals("scissors");
        }
        else {
            return choice_2.equals("paper");
        }

    }


    public String computerChoice() {
        Random random = new Random();
        int randomIndex = random.nextInt(rpsChoices.size());
        return rpsChoices.get(randomIndex);

    }


    public String playerChoice(String prompt, List<String> verifiedInput) {
        String playerChoice;
        while (true) {
            playerChoice = readInput(prompt);
            if (verifyInput(playerChoice,verifiedInput)) {
                break;
            }
            else {
                System.out.println("I do not understand" + playerChoice + ". Could you try again?");
            }
        }
        return playerChoice;
    }

    public boolean verifyInput(String input, List<String> verifiedInput) {
        return verifiedInput.contains(input);

    }




    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

}
